var gulp = require('gulp'); 
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');
var browserSync = require('browser-sync').create();

gulp.task('compile-stylus', function () {
    return gulp.src('./styl/style.styl')   
        .pipe(sourcemaps.init())
        .pipe(stylus()) 
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.stream());
});
 
gulp.task('stylus:watch', function () {   //jeżeli coś się zmieni w pliku, to uruchom compile-stylus
    gulp.watch('./styl/**/*.styl', gulp.series('compile-stylus'));
});


gulp.task('html:watch', function () {
    gulp.watch("./**/*.html").on('change', browserSync.reload); 
});

gulp.task('browsersync-init', function() {
    browserSync.init({
        server: "./"
        //port:7777 //-odpalamy np. na tym porcie
    });
})

gulp.task('default', gulp.parallel('browsersync-init','compile-stylus', 'stylus:watch', 'html:watch'));

